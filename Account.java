public abstract class Account {
    //properties
    private double balance;
    private String name;
    //methods
    public void setBalance(double balance){
        this.balance = balance;
    }
    public void setName(String name){
        this.name = name;
    }
    public double getBalance(){
        return balance;
    }
    public String getName(){
        return name;
    }
    private static double interestRate;
    public static void setInterest(double interest){
        interestRate = interest;
    }
    public static double getInterest(){
        return interestRate;
    }
    public abstract void addInterest();
        //for TestAccount.java
        //balance = balance * 1.1;

        //for TestAccount2.java
        //balance = balance * (1 + getInterest());


        //setBalance(getBalance() * 1.1);
    

    // constructor
    
    // no argument constructor
    

    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }
    
    public Account(){
        this.name = "Isabella";
        this.balance = 50;
    }
    
    //Method Overloading
    public boolean withdraw(double amount){
        if(this.balance >= amount){
            this.balance -= amount;
            System.out.println("Withdraw successfully, now balance is: " + balance);
            return true;
        }
        else {
            System.out.println("Withdraw failed");
            return false;
            
        }
    }
    public boolean withdraw(){
        if(this.balance >= 20){
            this.balance -= 20;
            System.out.println("Withdraw successfully, now balance is: " + balance);
            return true;
        }
        else {
            System.out.println("Withdraw failed");
            return false; 
        }
    }
    

}


