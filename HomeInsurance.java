public class HomeInsurance implements Detailable{
    private double premium;
    private double excess;
    
    public HomeInsurance(double premium, double excess, double amount) {
        this.premium = premium;
        this.excess = excess;
    }

    @Override
    public String getDetails() {
        // TODO Auto-generated method stub
        // the initial empty String promotes all the values to a String
        return "" + premium + " " + excess;
    }

}