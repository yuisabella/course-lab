public class CurrentAccount extends Account implements Detailable{
    public CurrentAccount(double balance, String name){
        super (balance, name);  //must be first line in constructor
    }

    
    @Override
    public void addInterest(){
            setBalance(getBalance() * 1.1);
    }

    @Override
    public String getDetails() {
        return "" + "Current account" + getBalance(); 
    }
}