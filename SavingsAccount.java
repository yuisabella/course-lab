public class SavingsAccount extends Account implements Detailable{

    public SavingsAccount(double balance, String name) {
        super (balance, name); //call to superclass constructor
    }

    
    @Override
    public void addInterest(){
        setBalance(getBalance() * 1.4);
    }

    @Override
    public String getDetails() {
        
        return "" + "Savings account balance" + getBalance();
    }
    
}