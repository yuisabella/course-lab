public class FirstTask {
    public static void main(String[] args) {
        String make = "BMW" ;
        String model = "530D";
        double engineSize = 3.0;
        byte gear = 2;
        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize );
        System.out.println("The gear is " + gear);
        short speed = (short)(gear * 20);
        System.out.println("The speed is " + speed);
        int revs = speed * gear;
        System.out.println("The revs is " + revs);
        //Using if/else
        if(engineSize <= 1.3){
            System.out.println("The car is a weak car");
        }
        else{
            System.out.println("The car is a powerful car");
        }

        if(gear == 5){
            speed = 50;
        }
        else if(gear == 1){
            speed = 9;
        }

        for(int i = 1900; i <= 2000; i++){
            
            if(i == 1905){
                System.out.println("finished");
                break;
            }

            System.out.println(i);
        }
        gear = 1;
        switch(gear){
            case 1: 
                speed = 50;
                System.out.println("Speed is " + speed);
                break;
            case 5: 
                speed = 9;
                System.out.println("Speed is " + speed);
                break;
            default: 
                speed = 40;
                System.out.println("Speed is " + speed);
        }
        

        
    }
}