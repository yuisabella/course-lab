public class TestInheritance {
    public static void main(String[] args) {
        //array type is abstract, contains objects of various subclass types
        Account accountoftypeAcount[] = new Account[3];
        accountoftypeAcount[0] = new SavingsAccount(2, "Rose");
        accountoftypeAcount[1] = new SavingsAccount(4, "Isabella");
        accountoftypeAcount[2] = new CurrentAccount(6, "Jessica");
        for(int i = 0; i < accountoftypeAcount.length; i++){

            accountoftypeAcount[i].addInterest();
            System.out.println("Name: " + accountoftypeAcount[i].getName() + " Balance: " + accountoftypeAcount[i].getBalance());
        }
    } 

}