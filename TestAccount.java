
public class TestAccount {
    public static void main(String[] args) {
        Account myAccount = new Account();
        myAccount.setBalance(50000);
        myAccount.setName("Isabella");
        System.out.println("My account name is " + myAccount.getName());
        System.out.println("My account balance is " + myAccount.getBalance());
        myAccount.addInterest();
        System.out.println("Now my account balance is "  + myAccount.getBalance());
        // add Chapter 7
        Account[] arrayOfAccounts = new Account[5];
        //create the array contents
        for(int i = 0; i < arrayOfAccounts.length; i++){
             arrayOfAccounts[i] = new Account();
        }
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
        for(int i = 0; i < arrayOfAccounts.length; i++){
            arrayOfAccounts[i].setBalance(amounts[i]);
            arrayOfAccounts[i].setName(names[i]);

            System.out.println("account name: " + arrayOfAccounts[i].getName() + ", account balance: " + arrayOfAccounts[i].getBalance());
        
            arrayOfAccounts[i].addInterest();
            System.out.println("After interest, account name: " + arrayOfAccounts[i].getName() + ", account balance: " + arrayOfAccounts[i].getBalance());
    
        }
            
        
        
    }
}